#include "../s21_string.h"

/*
преобразует указатель str в указатель на unsigned char, чтобы иметь возможность
записывать отдельные байты. Затем она заполняет первые n байтов значениями c.
*/

void *s21_memset(void *str, int c, size_t n) {
  unsigned char *byte_ptr = (unsigned char *)str;

  for (size_t i = 0; i < n; i++) {
    byte_ptr[i] = (unsigned char)c;
  }

  return str;
}
