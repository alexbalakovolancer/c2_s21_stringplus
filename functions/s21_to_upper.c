#include "../s21_string.h"

void *s21_to_upper(const char *str) {
  if (str != s21_NULL) {
    size_t length = s21_strlen(str);
    char *res = (char *)malloc((length + 1) * sizeof(char));
    if (res != s21_NULL) {
      for (size_t i = 0; i <= length; i++) {
        res[i] = str[i];
        if (res[i] >= 'a' && res[i] <= 'z') {
          res[i] -= 32;
        }
      }
      return res;
    }
  }
  return s21_NULL;
}
