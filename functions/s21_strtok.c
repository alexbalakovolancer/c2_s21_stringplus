#include "../s21_string.h"

// Разбивает строку str на ряд токенов, разделенных delim
char *s21_strtok(char *str, const char *delim) {
  static char *lastToken = s21_NULL;

  if (str != s21_NULL && s21_strncmp(str, "", 2) != 0) {
    lastToken = str;
  } else if (lastToken == s21_NULL) {
    return s21_NULL;
  }

  char *tokenStart = lastToken;
  char *tokenEnd = lastToken;

  while (*tokenEnd != '\0' && s21_strchr(delim, *tokenEnd) == s21_NULL) {
    tokenEnd++;
  }

  if (*tokenEnd != '\0') {
    *tokenEnd = '\0';
    lastToken = tokenEnd + 1;
  } else {
    lastToken = s21_NULL;
  }

  return tokenStart;
}
