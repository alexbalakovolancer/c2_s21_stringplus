#include "../s21_string.h"

// Копирует до n символов из строки, на которую указывает src, в dest.
char *s21_strncpy(char *dest, const char *src, size_t n) {
  size_t i;
  for (i = 0; i < n && src[i] != '\0'; i++) {
    dest[i] = src[i];
  }

  // Если скопировано менее n символов, дополняем dest нулевыми символами
  for (; i < n; i++) {
    dest[i] = '\0';
  }

  return dest;
}
