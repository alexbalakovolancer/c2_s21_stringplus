#include "../s21_string.h"

void *s21_trim(const char *src, const char *trim_chars) {
  char *trim_str = s21_NULL;
  if (src != s21_NULL) {
    const char *begin_str = src;
    const char *end_str = src + s21_strlen(src);

    // Обрезание начальных символов
    if (trim_chars == s21_NULL) {
      while (*begin_str && isspace((unsigned char)*begin_str)) {
        begin_str++;
      }
    } else {
      while (*begin_str && s21_strchr(trim_chars, *begin_str)) {
        begin_str++;
      }
    }

    // Обрезание конечных символов
    if (trim_chars == s21_NULL) {
      while (end_str != begin_str && isspace((unsigned char)*(end_str - 1))) {
        end_str--;
      }
    } else {
      while (end_str != begin_str && s21_strchr(trim_chars, *(end_str - 1))) {
        end_str--;
      }
    }

    trim_str = (char *)malloc(end_str - begin_str + 1);
    if (trim_str != s21_NULL) {
      s21_strncpy(trim_str, begin_str, end_str - begin_str);
      trim_str[end_str - begin_str] = '\0';
    }
  }
  return (void *)trim_str;
}
