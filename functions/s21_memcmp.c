#include "../s21_string.h"

/*
Сравнивает байты областей памяти str1 и str2 до n байтов.
Если обнаружено неравенство байтов, функция возвращает разницу.
Если области памяти равны, функция возвращает 0.
*/

int s21_memcmp(const void *str1, const void *str2, size_t n) {
  const unsigned char *byte_str1 = (const unsigned char *)str1;
  const unsigned char *byte_str2 = (const unsigned char *)str2;

  for (size_t i = 0; i < n; i++) {
    if (byte_str1[i] > byte_str2[i]) {
      return 1;
    } else if (byte_str1[i] < byte_str2[i]) {
      return -1;
    }
  }

  return 0;
}
