#include "../s21_string.h"

// Выполняет поиск первого вхождения символа c (беззнаковый тип) в строке, на
// которую указывает аргумент str.
char *s21_strchr(const char *str, int c) {
  char *res = s21_NULL;
  for (char *p = (char *)str; *p != '\0'; p++) {
    if (*p == (char)c) {
      res = p;
      break;
    }
  }
  return res;
}
