#include "../s21_string.h"

/*
Cравнивает символы строк str1 и str2 до n символов или до символа
нуль-терминатора '\0' в одной из строк. Если обнаружено неравенство символов,
функция возвращает разницу в ASCII-кодах символов. Если строки равны, функция
возвращает 0.
*/

int s21_strncmp(const char *str1, const char *str2, size_t n) {
  size_t i;

  for (i = 0; i < n && str1[i] != '\0' && str2[i] != '\0'; i++) {
    if (str1[i] != str2[i]) {
      return (int)(unsigned char)str1[i] - (int)(unsigned char)str2[i];
    }
  }

  if (i < n) {
    return (int)(unsigned char)str1[i] - (int)(unsigned char)str2[i];
  }

  return 0;
}
