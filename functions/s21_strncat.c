#include "../s21_string.h"

/*
находит длину строки dest, чтобы определить, где заканчивается она.
Затем она добавляет n символов из строки src в конец строки dest и добавляет
символ нуль-терминатора, чтобы строка оставалась корректно завершенной.
*/
char *s21_strncat(char *dest, const char *src, size_t n) {
  size_t dest_len = 0;
  while (dest[dest_len] != '\0') {
    dest_len++;
  }

  size_t i;
  for (i = 0; i < n && src[i] != '\0'; i++) {
    dest[dest_len + i] = src[i];
  }

  dest[dest_len + i] = '\0';

  return dest;
}
