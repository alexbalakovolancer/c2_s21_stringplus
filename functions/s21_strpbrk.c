#include "../s21_string.h"

char *s21_strpbrk(
    const char *str1,
    const char *str2)  // Находит первый символ в строке str1, который
                       // соответствует любому символу, указанному в str2.
{
  while (*str1 != '\0') {
    const char *c = str2;
    while (*c != '\0') {
      if (*c == *str1) {
        return (char *)str1;
      }
      c++;
    }
    str1++;
  }

  return s21_NULL;
}
