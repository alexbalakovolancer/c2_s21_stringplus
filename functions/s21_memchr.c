#include "../s21_string.h"

/*
Перебирает байты области памяти str до n байтов и ищет значение c.
Если значение найдено, функция возвращает указатель на найденный байт.
Если значение не найдено, функция возвращает s21_NULL.
*/

void *s21_memchr(const void *str, int c, size_t n) {
  const unsigned char *byte_str = (const unsigned char *)str;
  unsigned char target = (unsigned char)c;

  for (size_t i = 0; i < n; i++) {
    if (byte_str[i] == target) {
      return (void *)(byte_str + i);
    }
  }

  return s21_NULL;
}
