#include "../s21_string.h"

// Вычисляет длину начального сегмента str1, который полностью состоит из
// символов, не входящих в str2.
size_t s21_strcspn(const char *str1, const char *str2) {
  size_t count = 0;
  while (*str1 != '\0') {
    if (s21_strchr(str2, *str1) == s21_NULL) {
      count++;
    } else {
      break;
    }
    str1++;
  }
  return count;
}
