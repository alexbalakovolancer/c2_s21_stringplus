#include "../s21_string.h"

size_t s21_strlen(const char *str) {
  // Передаём указатель на первый элемент массива char
  char *p = (char *)str;
  size_t length = 0;
  while (*p != '\0') {
    length++;
    // Переходим к след. байту char (x[1] = *(x + 1) = x++)
    p++;
  }
  return length;
}
