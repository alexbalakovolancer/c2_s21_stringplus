#include "../s21_string.h"

void *s21_insert(const char *src, const char *str, size_t start_index) {
  if (src != s21_NULL && str != s21_NULL) {
    size_t size_src = s21_strlen(src);
    size_t size_str = s21_strlen(str);
    size_t size = size_src + size_str;

    if (start_index <= size_src) {
      char *ins_str = (char *)calloc((size + 1), sizeof(char));
      for (size_t i = 0, j = 0; i <= size; i++) {
        if (start_index == i) {
          while (j < size_str) {
            ins_str[i] = str[j];
            j++;
            i++;
          }
        }
        ins_str[i] = src[i - j];
      }
      s21_strncpy((char *)src, ins_str, size + 1);
      free(ins_str);
    }
  }
  return (void *)src;
}
