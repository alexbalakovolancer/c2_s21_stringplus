#include "../s21_string.h"

int s21_sscanf(const char *str, const char *format, ...) {
  int is_EOF = check_for_EOF(str);
  int res = 0;

  if (!is_EOF) {
    va_list arg_p;
    va_start(arg_p, format);

    char *format_c = (char *)format;
    char *str_c = (char *)str;
    int token_c = 0;
    int len = (int)s21_strlen(format_c);
    token tokens[BUFF_SIZE];

    for (int i = 0; i < len; i++) {
      tokens[token_c] = parse_token(&format_c, &arg_p);
      token_c++;
    }

    write_token(&str_c, tokens, token_c, &res);
    va_end(arg_p);
  }

  return is_EOF ? is_EOF : res;
}
