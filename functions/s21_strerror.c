#include "../s21_string.h"

char* s21_strerror(int errnum) {
  char* res = "";
  char* error_text = "Unknown error: ";
  char* mac_os_errors[107] = {MACOS_ERRORS};
  char* linux_errors[134] = {LINUX_ERRORS};
  if (MAC_OS) {
    if (errnum < 0 || errnum > 106) {
      char* code = int_to_char(errnum);
      int length = s21_strlen(error_text) + s21_strlen(code);
      res = malloc((length + 1) * sizeof(char));
      s21_strncat(res, error_text, s21_strlen(error_text));
      s21_strncat(res, code, s21_strlen(code));
    } else {
      res = mac_os_errors[errnum];
    }
  } else if (LINUX) {
    if (errnum < 0 || errnum > 133) {
      char* code = int_to_char(errnum);
      int length = s21_strlen(error_text) + s21_strlen(code);
      res = malloc((length + 1) * sizeof(char));
      s21_strncat(res, error_text, s21_strlen(error_text));
      s21_strncat(res, code, s21_strlen(code));
    } else {
      res = linux_errors[errnum];
    }
  }
  return res;
}
