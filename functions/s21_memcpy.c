#include "../s21_string.h"

// Копирует n символов из src в dest.
void *s21_memcpy(void *dest, const void *src, size_t n) {
  unsigned char *byte_dest = (unsigned char *)dest;
  const unsigned char *byte_src = (const unsigned char *)src;

  for (size_t i = 0; i < n && i < s21_strlen(src); i++) {
    byte_dest[i] = byte_src[i];
  }

  return dest;
}
