#include "../s21_string.h"

int s21_sprintf(char *str, const char *format, ...) {
  va_list p_args;
  va_start(p_args, format);

  copy_to_str(str, format, p_args);

  va_end(p_args);
  return s21_strlen(str);
}
