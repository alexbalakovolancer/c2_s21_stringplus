CC = gcc
CFLAGS = -std=c11 -Wall -Werror -Wextra
GCOVFLAGS = -fprofile-arcs -ftest-coverage
C_LIBS := -lm
TEST_LIBS:=
ifeq ($(shell uname -s),Linux)
	TESTFLAGS += -lm -lrt -lsubunit
endif
TEST_LIBS += -lcheck -lpthread -pthread

FUNCTION_SOURCES = $(wildcard functions/*.c)
FUNCTION_OBJECTS = $(patsubst %.c, %.o, $(FUNCTION_SOURCES))

SUPPORT_SOURCES = $(wildcard support/*.c)
SUPPORT_OBJECTS = $(patsubst %.c, %.o, $(SUPPORT_SOURCES))

TEST_SOURCES = $(wildcard test_lib/*.c test_lib/tests/*.c)
TEST_OBJECTS = $(patsubst %.c, %.o, $(TEST_SOURCES))

all: build

build: test s21_string.a gcov_report

test: $(TEST_OBJECTS) s21_string.a
	$(CC) $(CFLAGS) $(TEST_OBJECTS) s21_string.a $(TEST_LIBS) -o test
	./test

s21_string.a: $(FUNCTION_OBJECTS) $(SUPPORT_OBJECTS)
	ar rcs s21_string.a $(FUNCTION_OBJECTS) $(SUPPORT_OBJECTS)
	ranlib s21_string.a

debug: clean debug_flag build
	rm -rf *.o

gcov_report: test
	gcc $(CFLAGS) $(GCOVFLAGS) $(TEST_SOURCES) $(TEST_LIBS) $(SUPPORT_SOURCES) $(FUNCTION_SOURCES) -o gcovreport
	./gcovreport
	lcov -t "gcovreport" -o gcovreport.info -c -d .
	genhtml -o report gcovreport.info
	open report/index.html

clean:
	rm -rf *.a *.o *.gc* report gcovreport functions/*.o support/*.o test_lib/*.o test_lib/tests/*.o functions/*.gc* support/*.gc* test_lib/*.gc* test_lib/tests/*.gc* test test.gcda test.gcno *.info
