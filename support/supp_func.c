#include "../s21_string.h"

char *int_to_char(int c) {
  char *str = malloc(sizeof(char));
  int k = 1;
  if (c < 0) {
    k = -1;
    c *= -1;
  }
  int i = 0;
  while (c != 0) {
    str[i] = (c % 10) + '0';
    c /= 10;
    i++;
    str = realloc(str, sizeof(*str) + sizeof(char));
  }
  char *res = malloc(sizeof(*str) + sizeof(char));
  int sf = 0;
  if (k == -1) {
    res[0] = '-';
    sf = 1;
  }
  for (int j = i - 1; j >= 0; j--) {
    res[i - j - 1 + sf] = str[j];
  }
  free(str);
  return res;
}
