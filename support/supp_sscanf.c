#include "../s21_string.h"

int space_check(char c) { return (c == ' ' || c == '\n' || c == '\t'); }

int register_check(char c) {
  return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

int check_for_EOF(const char *str_c) {
  int result = -1;

  for (int i = 0; str_c[i]; i++) {
    if (!space_check(str_c[i]) && str_c[i] != '\0') {
      result = 0;
      break;
    }
  }

  return result;
}

void skip_chars_in_buffer(char **src_carriage, int *fail, token *t) {
  skip_spaces_in_str(src_carriage);

  int test = strspn_f(*src_carriage, t->buff);
  int length = s21_strlen(t->buff);

  if (test != length)
    *fail = 1;
  else
    (*src_carriage) = (*src_carriage) + length;
}

token parse_token(char **format, va_list *ap) {
  token t = {.address = s21_NULL,
             .token_length = NONE_LENGTH,
             .width = NONE_WIDTH,
             .spec = 0};

  if (space_check(**format)) {
    t.spec = ' ';  // тут пробел
    skip_spaces_in_str(format);
  }

  if (**format == '%' && !t.spec) {
    (*format)++;

    format_parse_width(format, &t);
    format_parse_length(format, &t);
    format_parse_specifier(format, &t);

    if (t.width != WIDTH_AST) t.address = va_arg(*ap, void *);

    if (t.spec == 'p') t.token_length = NONE_LENGTH;
  }

  if (register_check(**format) && !t.spec) {
    int i = 0;
    s21_memset(t.buff, '\0', 1024 - 1);
    while (**format && !space_check(**format) && **format != '%') {
      t.buff[i++] = **format;
      (*format)++;
    }
    t.spec = 'b';
  }
  return t;
}

void format_parse_width(char **format, token *t) {
  if (**format == '*') {
    (*format)++;
    t->width = WIDTH_AST;
  } else {
    int result = parse_number_from_format(format);

    if (result == 1) {
      t->width = WIDTH_NUMBER;
      t->width_status = result;
    }
  }
}

void format_parse_length(char **format, token *t) {
  switch (**format) {
    case 'h':
      t->token_length = LENGTH_SHORT;
      (*format)++;
      break;
    case 'l':
      t->token_length = LENGTH_LONG;
      (*format)++;
      if (**format == 'l') {
        t->token_length = LENGTH_LONG_LONG;
        (*format)++;
      }
      break;
    case 'L':
      t->token_length = LENGTH_LONG_DOUBLE;
      (*format)++;
      break;
  }
}

void format_parse_specifier(char **format, token *t) {
  t->spec = (**format);
  (*format)++;
}

void write_token(char **str_c, token *t, int token_c, int *result) {
  char *start_position = *str_c;

  for (int i = 0, fail_flag = 0; i < token_c && !fail_flag; i++) {
    char spec = t[i].spec;
    if (spec == 'c') write_char_to_memory(str_c, result, (t + i));
    if (spec == 'd') write_int_to_memory(str_c, &fail_flag, result, (t + i));
    if (spec == 'i')
      write_unspec_int_to_memory(str_c, &fail_flag, result, (t + i));
    if (spec == 'f' || spec == 'g' || spec == 'G')
      write_float_to_memory(str_c, result, (t + i));
    if (spec == 'o')
      write_8_16_to_memory(str_c, &fail_flag, result, (t + i), 8);
    if (spec == 's') write_string_to_memory(str_c, &fail_flag, result, (t + i));
    if (spec == 'u')
      write_unsigned_to_memory(str_c, &fail_flag, result, (t + i));
    if (spec == 'x' || spec == 'X' || spec == 'p')
      write_8_16_to_memory(str_c, &fail_flag, result, (t + i), 16);
    if (spec == 'n') *((int *)t[i].address) = (*str_c) - start_position;
    if (spec == ' ') skip_spaces_in_str(str_c);
    if (spec == 'b') skip_chars_in_buffer(str_c, &fail_flag, (t + i));
  }
}

int parse_number_from_format(char **format) {
  char tmp[BUFF_SIZE] = {'\0'};
  int result = 0, i = 0;

  // если это цифра, то добавляет ее в конец массива tmp
  while (**format >= '0' && **format <= '9') {
    tmp[i] = **format;
    (*format)++;
    i++;
  }
  result = atoi_f(tmp);
  return result;
}

void write_chars_to_buff(char **str_c, const char *c, char *buff, int16_t width,
                         int start_ind) {
  while (**str_c && strspn_f(*str_c, c) != 0) {
    if ((width && start_ind >= width) || (space_check(**str_c))) break;

    if (buff) buff[start_ind] = **str_c;

    (*str_c)++;
    start_ind++;
  }
}

void write_char_to_memory(char **str_c, int *result, token *t) {
  if (t->width == WIDTH_AST) {
    (*str_c)++;
  } else {
    *(char *)t->address = **str_c;
    (*str_c)++;
    (*result)++;
  }
}

void write_int_to_memory(char **str_c, int *fail_flag, int *res, token *t) {
  long long int result = 0;
  char buff[BUFF_SIZE] = {'\0'};
  *fail_flag = 1;

  if (strspn_f(*str_c, "0123456789+-")) {
    int sign = strspn_f(*str_c, "+-");

    if (!(sign > 1 || (sign && (t->width_status <= 1 && t->width)))) {
      buff[0] = **str_c;
      (*str_c)++;
      write_chars_to_buff(str_c, "0123456789", buff, t->width_status, 1);
      if (t->width != WIDTH_AST) (*res)++;
      *fail_flag = 0;
    }
  }
  result = atoi_f(buff);

  if (t->width != WIDTH_AST && !*fail_flag) int_type_converter(t, result, 1);

  if (t->width != WIDTH_NUMBER)
    write_chars_to_buff(str_c, "0123456789", s21_NULL, 0, 0);
}

void write_float_to_memory(char **str_c, int *result, token *t) {
  int test = 0;

  if (t->spec == 'f')
    test = strspn_f(*str_c, "0123456789+-");
  else
    test = strspn_f(*str_c, "0123456789eE+-NnaAifIF");

  if (test) {
    int sign = strspn_f(*str_c, "+-");
    if (!(sign > 1 || (sign && (t->width_status <= 1 && t->width)))) {
      char buff[BUFF_SIZE] = {'\0'};
      int start_index = 0;
      if (sign) {
        buff[0] = **str_c;
        start_index = 1;
        (*str_c)++;
      }
      if (t->spec == 'f')
        write_chars_to_buff(str_c, ".0123456789+-", buff, t->width_status,
                            start_index);
      else
        write_chars_to_buff(str_c, ".0123456789eE+-NnaAifIF", buff,
                            t->width_status, start_index);
      if (t->width != WIDTH_AST) {
        // конвертируем строку в double
        long double result_strtold = strtold_f(buff);
        (*result)++;
        float_type_converter(t, result_strtold);
      }
    }
  }

  if (t->width != WIDTH_NUMBER) {
    if (t->spec == 'f')
      write_chars_to_buff(str_c, ".0123456789", s21_NULL, 0, 0);
    else
      write_chars_to_buff(str_c, ".0123456789eE+-NaAifIFn", s21_NULL, 0, 0);
  }
}

void write_string_to_memory(char **str_c, const int *fail_flag, int *result,
                            token *t) {
  int success = 0;
  char buff[BUFF_SIZE] = {'\0'};
  unsigned int i = 0;

  while (**str_c != '\0' && !success && !(*fail_flag)) {
    if (!space_check(**str_c)) {
      success = 1;
      while (**str_c != '\0' && !(*fail_flag)) {
        buff[i] = **str_c;
        i++;

        if (t->width == WIDTH_NUMBER && i >= t->width_status) {
          break;
        }
        (*str_c)++;

        if (space_check(**str_c)) {
          (*str_c)--;
          break;
        }
      }
    }

    (*str_c)++;
  }

  if (t->width != WIDTH_AST && success) {
    strcpy_f((char *)t->address, buff);
    (*result)++;
  }
}

void write_unspec_int_to_memory(char **str_c, int *fail_flag, int *result,
                                token *t) {
  *fail_flag = 1;
  skip_spaces_in_str(str_c);

  if (strspn_f(*str_c, "+-0123456789")) {
    *fail_flag = 0;
    write_int_to_memory(str_c, fail_flag, result, t);
  } else if (strspn_f(*str_c, "0") == 1) {
    *fail_flag = 0;
    write_8_16_to_memory(str_c, fail_flag, result, t, 8);
  } else if (strspn_f(*str_c, "0x") == 2) {
    *fail_flag = 0;
    write_8_16_to_memory(str_c, fail_flag, result, t, 16);
  }
}

void write_unsigned_to_memory(char **str_c, int *fail_flag, int *result,
                              token *t) {
  *fail_flag = 1;
  skip_spaces_in_str(str_c);
  char buff[BUFF_SIZE] = {'\0'};

  if (strspn_f(*str_c, "0123456789+-")) {
    int sign = strspn_f(*str_c, "+-");
    if (!((sign > 1 || (sign && (t->width_status <= 1 && t->width))))) {
      *fail_flag = 0;
      buff[0] = **str_c;
      (*str_c)++;

      write_chars_to_buff(str_c, "0123456789", buff, t->width_status, 1);

      if (t->width != WIDTH_AST) (*result)++;
    }
  }
  unsigned long long int result_atoi = atoi_f(buff);

  if (t->width != WIDTH_AST && !*fail_flag)
    unsigned_type_converter(t, result_atoi, 1);

  if (t->width != WIDTH_NUMBER)
    write_chars_to_buff(str_c, "0123456789", s21_NULL, 0, 0);
}

void write_8_16_to_memory(char **str_c, int *fail_flag, int *result, token *t,
                          int base) {
  char *null_pointer = s21_NULL;
  int sign = 1;

  skip_spaces_in_str(str_c);

  if (**str_c == '-') {
    t->width_status--;
    sign = -1;
    (*str_c)++;
  }
  if (strspn_f(*str_c, "0123456789abcdefABCDEF") > 0 ||
      strspn_f(*str_c, "xX0123456789abcdefABCDEF") >= 2) {
    unsigned long long int res =
        strntollu_f(*str_c, &null_pointer, base,
                    t->width ? t->width_status : s21_strlen(*str_c));
    if (t->width != WIDTH_AST) {
      if (t->spec == 'p')
        *(int *)t->address = (int)res;
      else
        unsigned_type_converter(t, res, sign);
      *result += 1;
    } else {
      strntollu_f(*str_c, &null_pointer, base,
                  t->width ? t->width_status : s21_strlen(*str_c));
    }
  } else {
    *fail_flag = 1;
  }
  *str_c = null_pointer;
}

void skip_spaces_in_str(char **str_c) {
  while (**str_c && space_check(**str_c)) {
    (*str_c)++;
  }
}

void unsigned_type_converter(token *t, unsigned long long int result,
                             int sign) {
  if (t->token_length == NONE_LENGTH) {
    *(unsigned int *)t->address = sign * (unsigned int)result;
  } else if (t->token_length == LENGTH_SHORT) {
    *(unsigned short int *)t->address = sign * (unsigned short int)result;
  } else if (t->token_length == LENGTH_LONG) {
    *(unsigned long int *)t->address = sign * (unsigned long int)result;
  } else if (t->token_length == LENGTH_LONG_LONG) {
    *(unsigned long long int *)t->address =
        sign * (unsigned long long int)result;
  }
}

void int_type_converter(token *t, long long int result, int sign) {
  if (t->token_length == NONE_LENGTH) {
    *(int *)t->address = sign * (int)result;
  } else if (t->token_length == LENGTH_SHORT) {
    *(short int *)t->address = sign * (short int)result;
  } else if (t->token_length == LENGTH_LONG) {
    *(long int *)t->address = sign * (long int)result;
  } else if (t->token_length == LENGTH_LONG_LONG) {
    *(long long int *)t->address = sign * (long long int)result;
  }
}

void float_type_converter(token *t, long double result) {
  if (t->token_length == NONE_LENGTH) *(float *)t->address = (float)result;
  if (t->token_length == LENGTH_LONG) *(double *)t->address = (double)result;
  if (t->token_length == LENGTH_LONG_DOUBLE)
    *(long double *)t->address = (long double)result;
}

size_t strspn_f(const char *s, const char *accept) {
  const char *p;
  const char *a;
  size_t count = 0;

  for (p = s; *p != '\0'; p++) {
    for (a = accept; *a != '\0'; a++)
      if (*p == *a) break;
    if (*a == '\0')
      return count;
    else
      count++;
  }

  return count;
}

int atoi_f(const char *str_carriage) {
  int result = 0;
  int sign = 1;
  int overflow = 0;

  while (*str_carriage == ' ') str_carriage++;

  if (*str_carriage == '-') {
    str_carriage++;
    sign = -1;
  }

  if (*str_carriage == '+') {
    str_carriage++;
  }

  while (*str_carriage && *str_carriage >= '0' && *str_carriage <= '9') {
    result = result * 10 + (*str_carriage - '0');
    if (result < 0) {
      overflow = 1;
      break;
    }
    str_carriage++;
  }
  if (overflow)
    result = sign > 0 ? INT32_MAX : INT32_MIN;
  else
    result *= sign;

  return result;
}

char *strcpy_f(char *dest, const char *src) {
  int i = 0;
  while (src[i] != '\0') {
    dest[i] = src[i];
    i++;
  }
  dest[i] = '\0';
  return dest;
}

unsigned long long int strntollu_f(const char *string, char **endptr, int basis,
                                   int n_byte) {
  unsigned long long res = 0;
  short sign = 1;
  if (endptr) *endptr = s21_NULL;
  char dict[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  if (*string == '-') {
    sign = -1;
    string++;
  }
  if (basis == 16 &&
      (!s21_strncmp(string, "0x", 2) || !s21_strncmp(string, "0X", 2)))
    string += 2;
  long long val;
  short exit = 0;
  while (*string && n_byte && !exit) {
    char *tmp2;
    char current_sim =
        (*string >= 'a' && *string <= 'z') ? *string - 'a' + 'A' : *string;
    tmp2 = s21_strchr(dict, (int)current_sim);
    if (!tmp2) {
      exit = 1;
    } else {
      val = (tmp2 - dict) / sizeof(char);

      if (val >= basis) {
        exit = 1;
      } else {
        res = res * basis + val;
        string++;
        n_byte--;
      }
    }
    if (exit) *endptr = (char *)string;
  }
  return res * sign;
}

long double strtold_f(const char *buffer) {
  long double res = 0.0;
  int includes_inf_or_nan = includes_inf_nan(buffer);

  if (!includes_inf_or_nan) {
    res = atof_f(buffer);

    if (includes_exponent(buffer)) {
      res = apply_exponent(res, buffer);
    }
  }

  return includes_inf_or_nan ? return_nan_inf(buffer) : res;
}

int includes_inf_nan(const char *buffer) {
  int res = 0;

  int test1 = case_insens_search(buffer, "inf");
  int test2 = case_insens_search(buffer, "nan");

  if (test1 || test2) {
    res = 1;
  }

  return res;
}

int case_insens_search(const char *buff, const char *pat) {
  int found = 0;
  int len = (int)s21_strlen(pat);

  for (int i = 0; buff[i] && !found; i++) {
    int counter = 0;
    for (int j = 0; j < len; j++) {
      if ((buff[i] == (pat[j] - 'A') + 'a') ||
          (buff[i] == (pat[j] - 'a') + 'A') || pat[j] == buff[i]) {
        counter++;
        i++;
      }

      if (len == counter) {
        found = 1;
        break;
      }
    }
  }

  return found;
}

long double return_nan_inf(const char *buffer) {
  int res = 0;

  for (int i = 0; buffer[i]; i++) {
    if (buffer[i] == 'i' || buffer[i] == 'I') {
      res = 1;
      break;
    }

    if (buffer[i] == 'n' || buffer[i] == 'N') {
      res = 2;
      break;
    }
  }

  return (res == 1) ? INFINITY : NAN;
}

long double apply_exponent(long double res, const char *buffer) {
  char sign = '+';
  int expon = 0;

  for (char *p = (char *)buffer; *p; p++) {
    if (*p == 'e' || *p == 'E') {
      sign = *(p + 1);
      expon = atoi_f(p + 2);
    }
  }

  while (expon) {
    if (sign == '-') {
      res /= 10.0;
    } else {
      res *= 10.0;
    }
    expon--;
  }

  return res;
}

int includes_exponent(const char *buffer) {
  int res = 0;

  for (char *p = (char *)buffer; *p; p++) {
    if (strspn_f(p, "eE")) {
      res = 1;
      break;
    }
  }

  return res;
}

long double atof_f(const char *buffer) {
  long double frac = 0.0;
  char *p = (char *)buffer;
  int minus_flag = (*p == '-');
  if (*p == '-' || *p == '+') p++;

  long double res = atoi_f(p);

  while (isdigit_f(*p)) p++;

  if (*p == '.') {
    p++;
    int trailing_zeros = strspn_f(p, "0");
    frac = atoi_f(p);
    int tmp = (int)frac;
    while (tmp) {
      frac /= 10.0;
      tmp /= 10;
    }
    while (trailing_zeros) {
      frac /= 10.0;
      trailing_zeros--;
    }
  }

  res += frac;

  return minus_flag ? -res : res;
}

int isdigit_f(char c) {
  if (c >= '0' && c <= '9') {
    return 1;
  }
  return 0;
}
