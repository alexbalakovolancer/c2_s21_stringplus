#include "test.h"

int main() {
  int number_failed = 0;

  number_failed |= memchr_test();
  number_failed |= memcmp_test();
  number_failed |= memcpy_test();
  number_failed |= memset_test();
  number_failed |= strncat_test();
  number_failed |= strchr_test();
  number_failed |= strncmp_test();
  number_failed |= strncpy_test();
  number_failed |= strcspn_test();
  number_failed |= strerror_test();
  number_failed |= strlen_test();
  number_failed |= strpbrk_test();
  number_failed |= strrchr_test();
  number_failed |= strstr_test();
  number_failed |= strtok_test();
  number_failed |= sprintf_test();
  number_failed |= sscanf_test();
  number_failed |= trim_test();
  number_failed |= to_upper_test();
  number_failed |= to_lower_test();
  number_failed |= insert_test();

  return (number_failed == 0) ? 0 : 1;
}
