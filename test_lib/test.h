#ifndef TEST_H
#define TEST_H

#include <check.h>
#include <stdio.h>
#include <string.h>

#include "../s21_string.h"

int memchr_test();
int memcmp_test();
int memcpy_test();
int memset_test();
int strncat_test();
int strchr_test();
int strncmp_test();
int strncpy_test();
int strcspn_test();
int strerror_test();
int strlen_test();
int strpbrk_test();
int strrchr_test();
int strstr_test();
int strtok_test();
int sprintf_test();
int sscanf_test();
int trim_test();
int to_upper_test();
int to_lower_test();
int insert_test();

#endif  // TEST_H
