#include "../test.h"

START_TEST(memcpy_test_f) {
  {
    char *src = "12345";
    char dest_t[10] = "";
    char dest_r[10] = "";
    s21_memcpy(dest_t, src, 0);
    memcpy(dest_r, src, 0);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char *src = "123";
    char dest_t[10] = "12983";
    char dest_r[10] = "12983";
    s21_memcpy(dest_t, src, 3);
    memcpy(dest_r, src, 3);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char *src = "12345";
    char dest_t[10] = "";
    char dest_r[10] = "";
    s21_memcpy(dest_t, src, 6);
    memcpy(dest_r, src, 6);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char *src = "";
    char dest_t[10] = "";
    char dest_r[10] = "";
    s21_memcpy(dest_t, src, 3);
    memcpy(dest_r, src, 3);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
}
END_TEST

Suite *memcpy_suite(void) {
  Suite *memcpy = suite_create("memcpy");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, memcpy_test_f);
  suite_add_tcase(memcpy, tc_core);

  return memcpy;
}

int memcpy_test() {
  int number_failed = 0;
  Suite *memcpy = memcpy_suite();
  SRunner *sr = srunner_create(memcpy);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
