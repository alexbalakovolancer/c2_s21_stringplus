#include "../test.h"

START_TEST(strchr_test_f) {
  {
    char *str = "1234567";
    ck_assert(s21_strchr(str, '3') == strchr(str, '3'));
  }
  {
    char *str = "1234567";
    ck_assert(s21_strchr(str, '-') == strchr(str, '-'));
  }
  {
    char *str = "1234567";
    ck_assert(s21_strchr(str, ' ') == strchr(str, ' '));
  }
  {
    char *str = "1234567";
    ck_assert(s21_strchr(str, -1) == strchr(str, -1));
  }
}
END_TEST

Suite *strchr_suite(void) {
  Suite *strchr = suite_create("strchr");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strchr_test_f);
  suite_add_tcase(strchr, tc_core);

  return strchr;
}

int strchr_test() {
  int number_failed = 0;
  Suite *strchr = strchr_suite();
  SRunner *sr = srunner_create(strchr);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
