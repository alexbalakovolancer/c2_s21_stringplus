#include "../test.h"

START_TEST(strncmp_test_f) {
  {
    char *str1 = "1234567";
    char *str2 = "1234567";
    ck_assert(s21_strncmp(str1, str2, 7) == strncmp(str1, str2, 7));
  }
  {
    char *str1 = "1234567";
    char *str2 = "1234567";
    ck_assert(s21_strncmp(str1, str2, 0) == strncmp(str1, str2, 0));
  }
  {
    char *str1 = "1234567";
    char *str2 = "1234567";
    ck_assert(s21_strncmp(str1, str2, 2) == strncmp(str1, str2, 2));
  }
  {
    char *str1 = "";
    char *str2 = "";
    ck_assert(s21_strncmp(str1, str2, 2) == strncmp(str1, str2, 2));
  }
  {
    ck_assert(s21_strncmp(NULL, NULL, 0) == 0);  // Comparison with NULL
  }
  {
    char *str1 = "  ";
    char *str2 = "  ";
    ck_assert(s21_strncmp(str1, str2, 3) == strncmp(str1, str2, 3));
  }
}
END_TEST

Suite *strncmp_suite(void) {
  Suite *strncmp = suite_create("strncmp");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strncmp_test_f);
  suite_add_tcase(strncmp, tc_core);

  return strncmp;
}

int strncmp_test() {
  int number_failed = 0;
  Suite *strncmp = strncmp_suite();
  SRunner *sr = srunner_create(strncmp);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
