#include "../test.h"

START_TEST(insert_test_1) {
  char src[50] = "Hello, !";
  const char insert_str[] = "World";
  s21_insert(src, insert_str, 7);
  char *expected = "Hello, World!";
  ck_assert_str_eq(src, expected);
}
END_TEST

START_TEST(insert_test_2) {
  char src[50] = "Hello, !";
  const char insert_str[] = "World";
  s21_insert(src, insert_str, 0);
  char *expected = "WorldHello, !";
  ck_assert_str_eq(src, expected);
}
END_TEST

START_TEST(insert_test_3) {
  char src[50] = "";
  const char insert_str[] = "Hello";
  s21_insert(src, insert_str, 0);
  char *expected = "Hello";
  ck_assert_str_eq(src, expected);
}
END_TEST

Suite *insert_suite(void) {
  Suite *insert = suite_create("insert");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, insert_test_1);
  tcase_add_test(tc_core, insert_test_2);
  tcase_add_test(tc_core, insert_test_3);
  suite_add_tcase(insert, tc_core);

  return insert;
}

int insert_test() {
  int number_failed = 0;
  Suite *insert = insert_suite();
  SRunner *sr = srunner_create(insert);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
