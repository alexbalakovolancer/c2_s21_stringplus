#include "../test.h"

START_TEST(strrchr_test_f) {
  {
    char* str = "1234567";
    ck_assert(s21_strrchr(str, '3') == strrchr(str, '3'));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strrchr(str, '-') == strrchr(str, '-'));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strrchr(str, ' ') == strrchr(str, ' '));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strrchr(str, -1) == strrchr(str, -1));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strrchr(str, 150) == strrchr(str, 150));
  }
}
END_TEST

Suite* strrchr_suite(void) {
  Suite* strrchr = suite_create("strrchr");
  TCase* tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strrchr_test_f);
  suite_add_tcase(strrchr, tc_core);

  return strrchr;
}

int strrchr_test() {
  int number_failed = 0;
  Suite* strrchr = strrchr_suite();
  SRunner* sr = srunner_create(strrchr);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
