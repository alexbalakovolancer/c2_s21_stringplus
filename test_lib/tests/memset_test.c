#include "../test.h"

START_TEST(memset_test_f) {
  {
    char dest_t[6] = "12345";
    char dest_r[6] = "12345";
    s21_memset(dest_t, '1', 4);
    memset(dest_r, '1', 4);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char dest_t[5] = "1234";
    char dest_r[5] = "1234";
    s21_memset(dest_t, '1', 4);
    memset(dest_r, '1', 4);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char dest_t[5] = "";
    char dest_r[5] = "";
    s21_memset(dest_t, '1', 4);
    memset(dest_r, '1', 4);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
}
END_TEST

Suite *memset_suite(void) {
  Suite *memset = suite_create("memset");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, memset_test_f);
  suite_add_tcase(memset, tc_core);

  return memset;
}

int memset_test() {
  int number_failed = 0;
  Suite *memset = memset_suite();
  SRunner *sr = srunner_create(memset);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
