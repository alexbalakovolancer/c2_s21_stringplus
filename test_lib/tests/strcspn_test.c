#include "../test.h"

START_TEST(strcspn_test_f) {
  {
    char* str = "1234567";
    ck_assert(s21_strcspn(str, "123") == strcspn(str, "123"));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strcspn(str, "321") == strcspn(str, "321"));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strcspn(str, "34") == strcspn(str, "34"));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strcspn(str, "abc") == strcspn(str, "abc"));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strcspn(str, "abcef4") == strcspn(str, "abc4fe"));
  }
}
END_TEST

Suite* strcspn_suite(void) {
  Suite* strcspn = suite_create("strcspn");
  TCase* tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strcspn_test_f);
  suite_add_tcase(strcspn, tc_core);

  return strcspn;
}

int strcspn_test() {
  int number_failed = 0;
  Suite* strcspn = strcspn_suite();
  SRunner* sr = srunner_create(strcspn);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
