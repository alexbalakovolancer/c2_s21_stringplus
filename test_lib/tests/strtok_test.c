#include "../test.h"

START_TEST(strtok_test_f) {
  {
    char str[50] = "test1/test2/test3";
    char sep[2] = "/";
    ck_assert(s21_strtok(str, sep) == strtok(str, sep));
  }
  {
    char str[50] = "test1abctest2abctest3";
    char sep[4] = "abc";
    ck_assert(s21_strtok(str, sep) == strtok(str, sep));
  }
  {
    char* str = "test1/test2/test3";
    char* sep = "abc";
    ck_assert(s21_strtok(str, sep) == strtok(str, sep));
  }
  {
    char* str = "";
    char* sep = "abc";
    ck_assert(s21_strtok(str, sep) == strtok(str, sep));
  }
  {
    char* str = "test1abctest2abctest3";
    char* sep = "";
    ck_assert(s21_strtok(str, sep) == strtok(str, sep));
  }
}
END_TEST

Suite* strtok_suite(void) {
  Suite* strtok = suite_create("strtok");
  TCase* tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strtok_test_f);
  suite_add_tcase(strtok, tc_core);

  return strtok;
}

int strtok_test() {
  int number_failed = 0;
  Suite* strtok = strtok_suite();
  SRunner* sr = srunner_create(strtok);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
