#include "../test.h"

START_TEST(sprintf_test_f) {
  {
    char result[20];
    char assert[20];
    int d = 57;
    s21_sprintf(result, "Count: %d abc", d);
    sprintf(assert, "Count: %d abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    int d = 190;
    s21_sprintf(result, "Count: %5d", d);
    sprintf(assert, "Count: %5d", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    char c = 'a';
    s21_sprintf(result, "Count: %*c", 4, c);
    sprintf(assert, "Count: %*c", 4, c);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    char c = 'a';
    s21_sprintf(result, "Count: %-3c", c);
    sprintf(assert, "Count: %-3c", c);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20] = {'\0'};
    char assert[20] = {'\0'};
    char c = 'z';
    s21_sprintf(result, "Count: %lc", c);
    sprintf(assert, "Count: %lc", c);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    s21_sprintf(result, "Count: %d%d%d%d", -4, 8, 15, 16);
    sprintf(assert, "Count: %d%d%d%d", -4, 8, 15, 16);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    s21_sprintf(result, "Count: %1.1d", 69);
    sprintf(assert, "Count: %1.1d", 69);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    s21_sprintf(result, "Hello, world!");
    sprintf(assert, "Hello, world!");
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    s21_sprintf(result, "Count: %-5d", 69);
    sprintf(assert, "Count: %-5d", 69);
    ck_assert_str_eq(result, assert);
  }
  {
    char c = 'B';
    char result[20];
    char assert[20];
    s21_sprintf(result, "Char: %c%c%c", c, c, c);
    sprintf(assert, "Char: %c%c%c", c, c, c);
    ck_assert_str_eq(result, assert);
  }
  {
    char c = 'B';
    char result[20];
    char assert[20];
    s21_sprintf(result, "Char: %c", c);
    sprintf(assert, "Char: %c", c);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    int d = 571212;
    s21_sprintf(result, "Count: %.5d abc", d);
    sprintf(assert, "Count: %.5d abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    int d = 5312112;
    s21_sprintf(result, "Count: %-.5d abc", d);
    sprintf(assert, "Count: %-.5d abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    int d = 1;
    s21_sprintf(result, "Count: % d abc", d);
    sprintf(assert, "Count: % d abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    int d = 2147483647;
    s21_sprintf(result, "Count: % d abc", d);
    sprintf(assert, "Count: % d abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    int d = -2147483648;
    s21_sprintf(result, "Count: % d abc", d);
    sprintf(assert, "Count: % d abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    long int d = 9223372036854775807;
    s21_sprintf(result, "Count: %ld abc", d);
    sprintf(assert, "Count: %ld abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    short int d = 32767;
    s21_sprintf(result, "Count: %hd abc", d);
    sprintf(assert, "Count: %hd abc", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    int d = 327710;
    s21_sprintf(result, "Count: %11.10d end", d);
    sprintf(assert, "Count: %11.10d end", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    int i = 57;
    s21_sprintf(result, "Count: %i 1", i);
    sprintf(assert, "Count: %i 1", i);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    int i = 190;
    s21_sprintf(result, "Count: %5i", i);
    sprintf(assert, "Count: %5i", i);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    s21_sprintf(result, "Count: %i%i%i%i", -4, 8, 15, 16);
    sprintf(assert, "Count: %i%i%i%i", -4, 8, 15, 16);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    s21_sprintf(result, "Count: %1.1i", 69);
    sprintf(assert, "Count: %1.1i", 69);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20];
    char assert[20];
    int i = 5312112;
    s21_sprintf(result, "Count: %-.5i abc", i);
    sprintf(assert, "Count: %-.5i abc", i);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    long int i = 214748364912;
    s21_sprintf(result, "Count: % li abc", i);
    sprintf(assert, "Count: % li abc", i);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    long int i = -9223372036854775807;
    s21_sprintf(result, "Count: % li abc", i);
    sprintf(assert, "Count: % li abc", i);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    short int i = 32767;
    s21_sprintf(result, "Count: %hi abc", i);
    sprintf(assert, "Count: %hi abc", i);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    int i = 327710;
    s21_sprintf(result, "Count: %-+10.5i end", i);
    sprintf(assert, "Count: %-+10.5i end", i);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    int i = 327710;
    s21_sprintf(result, "Count: %+10.5i end", i + 1);
    sprintf(assert, "Count: %+10.5i end", i + 1);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    float f = -14.537;
    s21_sprintf(result, "Count: %.6f end", f);
    sprintf(assert, "Count: %.6f end", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    float f = 14.537;
    s21_sprintf(result, "Count: %2.2f end", f);
    sprintf(assert, "Count: %2.2f end", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    float f = 1.0010021;
    s21_sprintf(result, "Count: %f end", f);
    sprintf(assert, "Count: %f end", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    float f = 0.;
    s21_sprintf(result, "Count: %f end", f);
    sprintf(assert, "Count: %f end", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    float f = 1.1234;
    s21_sprintf(result, "Count: %.*f end", 3, f);
    sprintf(assert, "Count: %.*f end", 3, f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    float f = -.0000001;
    s21_sprintf(result, "Count: %f end", f);
    sprintf(assert, "Count: %f end", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    float f = -.0000001;
    s21_sprintf(result, "Count: %10f end", f);
    sprintf(assert, "Count: %10f end", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    double f = -.0000001;
    s21_sprintf(result, "Count: %3.5lf end", f);
    sprintf(assert, "Count: %3.5lf end", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%Lf";
    long double val = 513515.131513515151351;
    int a = s21_sprintf(str1, format, val);
    int b = sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
    ck_assert_int_eq(a, b);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%10Lf";
    long double val = 15.35;
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.0Lf";
    long double val = 15.35;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.Lf";
    long double val = 15.35;
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.15Lf";
    long double val = 15.35;
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%Lf";
    long double val = 72537572375.1431341;
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "% f";
    float val = 0;
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "% .0f %.lf %Lf %f %lf %Lf";
    float val = 0;
    double val1 = 0;
    long double val2 = 3515315.153151;
    float val3 = 5.5;
    double val4 = 9851.51351;
    long double val5 = 95919539159.53151351131;
    ck_assert_int_eq(
        s21_sprintf(str1, format, val, val1, val2, val3, val4, val5),
        sprintf(str2, format, val, val1, val2, val3, val4, val5));
    ck_assert_str_eq(str1, str2);
  }
  {
    char result[40];
    char assert[40];
    float f = 14.537;
    s21_sprintf(result, "%+0.2f", f);
    sprintf(assert, "%+0.2f", f);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    char *s = "none";
    s21_sprintf(result, "Count: %s abc", s);
    sprintf(assert, "Count: %s abc", s);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    char *s = "none";
    s21_sprintf(result, "Count: %.3s abc", s);
    sprintf(assert, "Count: %.3s abc", s);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    char *s = "none";
    s21_sprintf(result, "%.6s", s);
    sprintf(assert, "%.6s", s);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    char *s = "stroka s probelom";
    s21_sprintf(result, "%5.6s", s);
    sprintf(assert, "%5.6s", s);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[40];
    char assert[40];
    wchar_t s[] = L"wide chars: ";
    s21_sprintf(result, "%-5.6ls", s);
    sprintf(assert, "%-5.6ls", s);
    ck_assert_str_eq(result, assert);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%s";
    char *val = "21R DSADA SDHASDOAMDSA sdas8d7nasd 111";
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.15s";
    char *val = "21R DSADA SDHASDOAMDSA sdas8d7nasd 111";
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%15s";
    char *val = "21R DSADA SDHASDOAMDSA sdas8d7nasd 111";
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%s";
    char *val = "ADibsy8 ndASN) dun8AWn dA 9sDNsa NAID saDYBU DSnaJ Sd";
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%s%s%s%s";
    char *val = "ADibsy8 ndASN) dun8AWn dA 9sDNsa NAID saDYBU DSnaJ Sd";
    char *s1 = "";
    char *s2 = "87418347813748913749871389480913";
    char *s3 = "HAHAABOBASUCKER";
    ck_assert_int_eq(s21_sprintf(str1, format, val, s1, s2, s3),
                     sprintf(str2, format, val, s1, s2, s3));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *val = "kjafdiuhfjahfjdahf";
    char *format = "%120s";
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    ck_assert_int_eq(
        s21_sprintf(str1, "%s%s%s%s%s", "DASdw", " ", "sadw", " ", "ASD"),
        sprintf(str2, "%s%s%s%s%s", "DASdw", " ", "sadw", " ", "ASD"));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "wchar: %ls";
    wchar_t w[] = L"à";
    int a = s21_sprintf(str1, format, w);
    int b = sprintf(str2, format, w);
    ck_assert_str_eq(str1, str2);
    ck_assert_int_eq(a, b);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "wchar: %5.12ls";
    wchar_t w[] = L"àààààà";
    int a = s21_sprintf(str1, format, w);
    int b = sprintf(str2, format, w);
    ck_assert_str_eq(str1, str2);
    ck_assert_int_eq(a, b);
  }
  {
    char str1[BUFF_SIZE] = {'\0'};
    char str2[BUFF_SIZE] = {'\0'};
    char *format = "wchar: %-5lc";
    unsigned long w = L'~';
    int a = s21_sprintf(str1, format, w);
    int b = sprintf(str2, format, w);
    ck_assert_str_eq(str1, str2);
    ck_assert_int_eq(a, b);
  }
  {
    char result[20];
    char assert[20];
    unsigned int u = 5312112;
    s21_sprintf(result, "Count: %-.5u abc", u);
    sprintf(assert, "Count: %-.5u abc", u);
    ck_assert_str_eq(result, assert);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%g";
    double hex = 0.50300;
    ck_assert_int_eq(s21_sprintf(str1, format, hex),
                     sprintf(str2, format, hex));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%g";
    double hex = 5131.43;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%g";
    double hex = 0.1;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%.5g";
    double hex = 0.55555;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%.0g";
    double hex = 0.4;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%.g";
    double hex = 5;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%.g";
    double hex = 0.0004;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%g";
    double hex = 0;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%.5g";
    double hex = 0;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char format[] = "%5.8g";
    double hex = 0.0000005;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE] = {'\0'};
    char str2[BUFF_SIZE] = {'\0'};
    char format[] = "%LG";
    long double hex = 0.000005;
    s21_sprintf(str1, format, hex);
    sprintf(str2, format, hex);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE] = {'\0'};
    char str2[BUFF_SIZE] = {'\0'};
    char format[] = "%LG %g %G %Lg %.5g";
    long double hex = 0.000005;
    double hex1 = 41.1341;
    double hex2 = 848.9000;
    long double hex3 = 0.0843;
    double hex4 = 0.0005;
    double hex5 = 0.8481481;
    s21_sprintf(str1, format, hex, hex1, hex2, hex3, hex4, hex5);
    sprintf(str2, format, hex, hex1, hex2, hex3, hex4, hex5);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.17Le";
    long double val = 15.35;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.Le";
    long double val = 15.000009121;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.15Le";
    long double val = 0.000000000000000123;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%Le";
    long double val = 72537572375.1431341;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE] = {'\0'};
    char str2[BUFF_SIZE] = {'\0'};
    char *format = "%015E";
    float val = 0;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%15e";
    float val = 0;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "% .0e %.le %Le %e %le %Le";
    float val = 0;
    double val1 = 0;
    long double val2 = 3515315.153151;
    float val3 = 5.5;
    double val4 = 0.094913941;
    long double val5 = 95919539159.53151351131;
    s21_sprintf(str1, format, val, val1, val2, val3, val4, val5);
    sprintf(str2, format, val, val1, val2, val3, val4, val5);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE] = {'\0'};
    char str2[BUFF_SIZE] = {'\0'};
    char *format = "%.17LE";
    long double val = 4134121;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%15o";
    int val = 14140;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%-16o";
    int val = 14140;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.51o";
    int val = 14140;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%-5.51o";
    int val = 14140;
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#o";
    int val = 57175;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%lo";
    long int val = 84518;
    ck_assert_int_eq(s21_sprintf(str1, format, val),
                     sprintf(str2, format, val));
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#o";
    int val = -57175;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#- 10o";
    int val = -573375;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%5x";
    unsigned val = 858158158;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#-10x";
    unsigned val = 858158158;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.15x";
    unsigned val = 858158158;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#-5.10x";
    unsigned val = 858158158;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#x";
    unsigned val = 0;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#hx";
    unsigned short val = 12352;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%#30x";
    unsigned val = 1;
    s21_sprintf(str1, format, val);
    sprintf(str2, format, val);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%p";
    s21_sprintf(str1, format, format);
    sprintf(str2, format, format);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%15p";
    s21_sprintf(str1, format, format);
    sprintf(str2, format, format);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%.5p";
    s21_sprintf(str1, format, format);
    sprintf(str2, format, format);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    char *format = "%p";
    char *ptr = "(nil)";
    s21_sprintf(str1, format, ptr);
    sprintf(str2, format, ptr);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    int ret = 0;
    char *format = "How many chars written before n %n";
    s21_sprintf(str1, format, &ret);
    sprintf(str2, format, &ret);
    ck_assert_str_eq(str1, str2);
  }
  {
    char str1[BUFF_SIZE];
    char str2[BUFF_SIZE];
    int n1;
    int n2;
    s21_sprintf(str1, "%d%n", 123, &n1);
    sprintf(str2, "%d%n", 123, &n2);
    ck_assert_str_eq(str1, str2);
  }
  {
    char result[512];
    char assert[512];
    char *d = "2147";
    s21_sprintf(result, "%101s", d);
    sprintf(assert, "%101s", d);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[512];
    char assert[512];
    char c = 'z';
    s21_sprintf(result, "Count: %12lc", c);
    sprintf(assert, "Count: %12lc", c);
    ck_assert_str_eq(result, assert);
  }
  {
    char result[20] = {'\0'};
    char assert[20] = {'\0'};
    char c = 'z';
    s21_sprintf(result, "Count: %-12lc", c);
    sprintf(assert, "Count: %-12lc", c);
    ck_assert_str_eq(result, assert);
  }
  {
    char str1[BUFF_SIZE] = {'\0'};
    char str2[BUFF_SIZE] = {'\0'};
    char *format = "wchar: %lc";
    wchar_t w = L'A';
    int a = s21_sprintf(str1, format, w);
    int b = sprintf(str2, format, w);
    ck_assert_str_eq(str1, str2);
    ck_assert_int_eq(a, b);
  }
}
END_TEST

Suite *sprintf_suite(void) {
  Suite *sprintf = suite_create("sprintf");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, sprintf_test_f);
  suite_add_tcase(sprintf, tc_core);

  return sprintf;
}

int sprintf_test() {
  int number_failed = 0;
  Suite *sprintf = sprintf_suite();
  SRunner *sr = srunner_create(sprintf);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
