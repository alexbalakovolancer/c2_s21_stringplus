#include "../test.h"

START_TEST(memchr_test_f) {
  {
    char* str = "1234567";
    ck_assert(s21_memchr(str, '4', 5) == memchr(str, '4', 5));
  }
  {
    char* str = "1234567";
    ck_assert(s21_memchr(str, '0', 7) == memchr(str, '0', 7));
  }
  {
    char* str = "";
    ck_assert(s21_memchr(str, '0', 7) == memchr(str, '0', 7));
  }
  {
    char* str = "12";
    ck_assert(s21_memchr(str, '2', 6) == memchr(str, '2', 6));
  }
  {
    char* str = "12345";
    ck_assert(s21_memchr(str, '5', 2) == memchr(str, '5', 2));
  }
  {
    char* str = "12345";
    ck_assert(s21_memchr(str, '1', 0) == memchr(str, '1', 0));
  }
  {
    char* str = " ";
    ck_assert(s21_memchr(str, ' ', 1) == memchr(str, ' ', 1));
  }
}
END_TEST

Suite* memchr_suite(void) {
  Suite* memchr = suite_create("memchr");
  TCase* tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, memchr_test_f);
  suite_add_tcase(memchr, tc_core);

  return memchr;
}

int memchr_test() {
  int number_failed = 0;
  Suite* memchr = memchr_suite();
  SRunner* sr = srunner_create(memchr);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
