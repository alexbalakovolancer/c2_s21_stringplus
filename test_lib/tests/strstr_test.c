#include "../test.h"

START_TEST(strstr_test_f) {
  {
    char* str = "test1/test2/test3";
    char* sep = "/";
    ck_assert(s21_strstr(str, sep) == strstr(str, sep));
  }
  {
    char* str = "test1abctest2abctest3";
    char* sep = "abc";
    ck_assert(s21_strstr(str, sep) == strstr(str, sep));
  }
  {
    char* str = "test1/test2/test3";
    char* sep = "abc";
    ck_assert(s21_strstr(str, sep) == strstr(str, sep));
  }
  {
    char* str = "";
    char* sep = "abc";
    ck_assert(s21_strstr(str, sep) == strstr(str, sep));
  }
  {
    char* str = "test1abctest2abctest3";
    char* sep = "";
    ck_assert(s21_strstr(str, sep) == strstr(str, sep));
  }
}
END_TEST

Suite* strstr_suite(void) {
  Suite* strstr = suite_create("strstr");
  TCase* tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strstr_test_f);
  suite_add_tcase(strstr, tc_core);

  return strstr;
}

int strstr_test() {
  int number_failed = 0;
  Suite* strstr = strstr_suite();
  SRunner* sr = srunner_create(strstr);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
