#include "../test.h"

START_TEST(strncat_test_f) {
  {
    char dest_t[10] = "12345";
    char dest_r[10] = "12345";
    s21_strncat(dest_t, "1s345", 4);
    strncat(dest_r, "1s345", 4);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char dest_t[12] = "12345";
    char dest_r[12] = "12345";
    s21_strncat(dest_t, "12345", 6);
    strncat(dest_r, "12345", 6);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char dest_t[10] = "";
    char dest_r[10] = "";
    s21_strncat(dest_t, "1", 0);
    strncat(dest_r, "1", 0);
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
}
END_TEST

Suite *strncat_suite(void) {
  Suite *strncat = suite_create("strncat");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strncat_test_f);
  suite_add_tcase(strncat, tc_core);

  return strncat;
}

int strncat_test() {
  int number_failed = 0;
  Suite *strncat = strncat_suite();
  SRunner *sr = srunner_create(strncat);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
