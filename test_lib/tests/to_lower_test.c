#include "../test.h"

START_TEST(to_lower_test_1) {
  char str[] = "ThIs Is A TeSt";
  char *result = s21_to_lower(str);
  char *expected = "this is a test";
  ck_assert_str_eq(result, expected);
  free(result);
}
END_TEST

START_TEST(to_lower_test_2) {
  char str[] = "12345";
  s21_to_lower(str);
  char *expected = "12345";
  ck_assert_str_eq(str, expected);
}
END_TEST

START_TEST(to_lower_test_3) {
  char str[] = "";
  s21_to_lower(str);
  char *expected = "";
  ck_assert_str_eq(str, expected);
}
END_TEST
Suite *to_lower_suite(void) {
  Suite *to_lower = suite_create("to_lower");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, to_lower_test_1);
  tcase_add_test(tc_core, to_lower_test_2);
  tcase_add_test(tc_core, to_lower_test_3);
  suite_add_tcase(to_lower, tc_core);

  return to_lower;
}

int to_lower_test() {
  int number_failed = 0;
  Suite *to_lower = to_lower_suite();
  SRunner *sr = srunner_create(to_lower);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
