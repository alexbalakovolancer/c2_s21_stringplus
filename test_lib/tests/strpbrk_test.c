#include "../test.h"

START_TEST(strpbrk_test_f) {
  {
    char* str = "1234567";
    ck_assert(s21_strpbrk(str, "abc") == strpbrk(str, "abc"));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strpbrk(str, "321") == strpbrk(str, "321"));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strpbrk(str, "") == strpbrk(str, ""));
  }
  {
    char* str = "1234567";
    ck_assert(s21_strpbrk(str, "abc") == strpbrk(str, "abc"));
  }
  {
    char* str = "";
    ck_assert(s21_strpbrk(str, "123") == strpbrk(str, "123"));
  }
}
END_TEST

Suite* strpbrk_suite(void) {
  Suite* strpbrk = suite_create("strpbrk");
  TCase* tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strpbrk_test_f);
  suite_add_tcase(strpbrk, tc_core);

  return strpbrk;
}

int strpbrk_test() {
  int number_failed = 0;
  Suite* strpbrk = strpbrk_suite();
  SRunner* sr = srunner_create(strpbrk);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
