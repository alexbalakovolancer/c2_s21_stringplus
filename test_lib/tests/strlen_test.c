#include "../test.h"

START_TEST(strlen_test_f) {
  { ck_assert_int_eq(s21_strlen("1234567"), strlen("1234567")); }
  { ck_assert_int_eq(s21_strlen("12345"), strlen("12345")); }
  { ck_assert_int_eq(s21_strlen(""), strlen("")); }
}
END_TEST

Suite *strlen_suite(void) {
  Suite *strlen = suite_create("strlen");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strlen_test_f);
  suite_add_tcase(strlen, tc_core);

  return strlen;
}

int strlen_test() {
  int number_failed = 0;
  Suite *strlen = strlen_suite();
  SRunner *sr = srunner_create(strlen);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
