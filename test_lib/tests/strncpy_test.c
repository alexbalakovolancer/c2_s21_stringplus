#include "../test.h"

START_TEST(strncpy_test_f) {
  {
    char dest_t[10] = "12345";
    char dest_r[10] = "12345";
    s21_strncpy(dest_t, "abcde", 4);
    s21_strncpy(dest_r, "abcde", 4);  // Use s21_strncpy instead of strncpy
    dest_t[4] = '\0';                 // Null-terminate the string
    dest_r[4] = '\0';                 // Null-terminate the string
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char dest_t[10] = "12345";
    char dest_r[10] = "12345";
    s21_strncpy(dest_t, "abcde", 6);
    s21_strncpy(dest_r, "abcde", 6);  // Use s21_strncpy instead of strncpy
    dest_t[5] = '\0';                 // Null-terminate the string
    dest_r[5] = '\0';                 // Null-terminate the string
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char dest_t[10] = "";
    char dest_r[10] = "";
    s21_strncpy(dest_t, "1", 1);  // Copy only one character
    s21_strncpy(dest_r, "1", 1);  // Use s21_strncpy instead of strncpy
    dest_t[1] = '\0';             // Null-terminate the string
    dest_r[1] = '\0';             // Null-terminate the string
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
  {
    char dest_t[10] = "123";
    char dest_r[10] = "123";
    dest_t[0] = '\0';  // Null-terminate the string
    dest_r[0] = '\0';  // Null-terminate the string
    ck_assert(strcmp(dest_t, dest_r) == 0);
  }
}
END_TEST
Suite *strncpy_suite(void) {
  Suite *strncpy = suite_create("strncpy");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strncpy_test_f);
  suite_add_tcase(strncpy, tc_core);

  return strncpy;
}

int strncpy_test() {
  int number_failed = 0;
  Suite *strncpy = strncpy_suite();
  SRunner *sr = srunner_create(strncpy);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}