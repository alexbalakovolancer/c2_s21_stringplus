#include "../test.h"

START_TEST(to_upper_test_1) {
  char str[] = "ThIs Is A TeSt";
  char *result = s21_to_upper(str);
  char *expected = "THIS IS A TEST";
  ck_assert_str_eq(result, expected);
  free(result);
}
END_TEST

START_TEST(to_upper_test_2) {
  char str[] = "12345";
  s21_to_upper(str);
  char *expected = "12345";
  ck_assert_str_eq(str, expected);
}
END_TEST

START_TEST(to_upper_test_3) {
  char str[] = "";
  s21_to_upper(str);
  char *expected = "";
  ck_assert_str_eq(str, expected);
}
END_TEST

START_TEST(to_upper_test_4) {
  char *result = s21_to_upper(s21_NULL);
  ck_assert_ptr_eq(result, s21_NULL);
}
END_TEST

Suite *to_upper_suite(void) {
  Suite *to_upper = suite_create("to_upper");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, to_upper_test_1);
  tcase_add_test(tc_core, to_upper_test_2);
  tcase_add_test(tc_core, to_upper_test_3);
  tcase_add_test(tc_core, to_upper_test_4);
  suite_add_tcase(to_upper, tc_core);

  return to_upper;
}

int to_upper_test() {
  int number_failed = 0;
  Suite *to_upper = to_upper_suite();
  SRunner *sr = srunner_create(to_upper);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
