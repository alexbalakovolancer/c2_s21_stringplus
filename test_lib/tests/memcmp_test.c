#include "../test.h"

START_TEST(memcmp_test_f) {
  {
    ck_assert_int_eq(s21_memcmp("1234567", "2345", 5),
                     memcmp("1234567", "2345", 5));
  }
  {
    ck_assert_int_eq(s21_memcmp("1234567", "-", 1), memcmp("1234567", "-", 1));
  }
  { ck_assert_int_eq(s21_memcmp("", "", 0), memcmp("", "", 0)); }
  { ck_assert_int_eq(s21_memcmp("12", "2", 1), memcmp("12", "2", 1)); }
  { ck_assert_int_eq(s21_memcmp("12345", "", 0), memcmp("12345", "", 0)); }
  { ck_assert_int_eq(s21_memcmp("12345", "1", 0), memcmp("12345", "1", 0)); }
  { ck_assert_int_eq(s21_memcmp(" ", " ", 1), memcmp(" ", " ", 1)); }
}
END_TEST

Suite *memcmp_suite(void) {
  Suite *memcmp = suite_create("memcmp");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, memcmp_test_f);
  suite_add_tcase(memcmp, tc_core);

  return memcmp;
}

int memcmp_test() {
  int number_failed = 0;
  Suite *memcmp = memcmp_suite();
  SRunner *sr = srunner_create(memcmp);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
