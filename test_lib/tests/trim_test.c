#include "../test.h"

START_TEST(trim_test_1) {
  char src[] = "   Hello, World!   ";
  const char *trim_chars = " ";
  char *result = s21_trim(src, trim_chars);
  char *expected = "Hello, World!";
  ck_assert_str_eq(result, expected);
  free(result);  // Не забудьте освободить память после использования
}
END_TEST

START_TEST(trim_test_2) {
  char src[] = "\t\n  \vHello, World!\t  \n\v";
  char *result = s21_trim(src, s21_NULL);
  char *expected = "Hello, World!";
  ck_assert_str_eq(result, expected);
  free(result);
}
END_TEST

START_TEST(trim_test_3) {
  char src[] = "  ";
  char *result = s21_trim(src, s21_NULL);
  char *expected = "";
  ck_assert_str_eq(result, expected);
  free(result);
}
END_TEST

Suite *trim_suite(void) {
  Suite *trim = suite_create("trim");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, trim_test_1);
  tcase_add_test(tc_core, trim_test_2);
  tcase_add_test(tc_core, trim_test_3);
  suite_add_tcase(trim, tc_core);

  return trim;
}

int trim_test() {
  int number_failed = 0;
  Suite *trim = trim_suite();
  SRunner *sr = srunner_create(trim);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
