#include "../test.h"

START_TEST(sscanf_test_f) {
  {
    int x, y;
    int result = s21_sscanf("123 456", "%d %d", &x, &y);
    ck_assert_int_eq(result, 2);
    ck_assert_int_eq(x, 123);
    ck_assert_int_eq(y, 456);
  }
  {
    float x, y;
    int result = s21_sscanf("3.14 -1.23", "%f %f", &x, &y);
    ck_assert_int_eq(result, 2);
    ck_assert_float_eq_tol(x, 3.14, 0.01);
    ck_assert_float_eq_tol(y, -1.23, 0.01);
  }
  {
    char x[20], y[20];
    int result = s21_sscanf("hello world", "%s %s", x, y);
    ck_assert_int_eq(result, 2);
    ck_assert_str_eq(x, "hello");
    ck_assert_str_eq(y, "world");
  }
  {
    char x[20];
    int result = s21_sscanf(" abc ", "%s", x);
    ck_assert_int_eq(result, 1);
    ck_assert_str_eq(x, "abc");
  }
  {
    int x;
    float y;
    char z[20];
    int result = s21_sscanf("123 4.56 hello", "%d %f %s", &x, &y, z);
    ck_assert_int_eq(result, 3);
    ck_assert_int_eq(x, 123);
    ck_assert_float_eq_tol(y, 4.56, 0.01);
    ck_assert_str_eq(z, "hello");
  }
  {
    char x[20], y[20];
    int result = s21_sscanf("hello world", "%5s %5s", x, y);
    ck_assert_int_eq(result, 2);
    ck_assert_str_eq(x, "hello");
    ck_assert_str_eq(y, "world");
  }
  {
    int x, y;
    int result = s21_sscanf("123 456", "%d%n %d", &x, &y, &y);
    ck_assert_int_eq(result, 2);
    ck_assert_int_eq(x, 123);
    ck_assert_int_eq(y, 456);
  }
  {
    uint32_t a1, a2;
    const char str[] = "          \n             \n     5";
    const char fstr[] = "%o";
    uint32_t res1 = s21_sscanf(str, fstr, &a1);
    uint32_t res2 = sscanf(str, fstr, &a2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
  }
  {
    uint16_t a1, a2;
    const char str[] = "12";
    const char fstr[] = "%ho";
    uint16_t res1 = s21_sscanf(str, fstr, &a1);
    uint16_t res2 = sscanf(str, fstr, &a2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
  }
  {
    unsigned short a1 = 0, a2 = 0, b1 = 0, b2 = 0, c1 = 0, c2 = 0, d1 = 0,
                   d2 = 0;
    const char str[] = "-1337 +21 --5008 3000";
    const char fstr[] = "%hu %hu %hu %hu";

    int16_t res1 = s21_sscanf(str, fstr, &a1, &b1, &c1, &d1);
    int16_t res2 = sscanf(str, fstr, &a2, &b2, &c2, &d2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
    ck_assert_int_eq(b1, b2);
    ck_assert_int_eq(c1, c2);
    ck_assert_int_eq(d1, d2);
  }
  {
    long long a1 = 0, a2 = 0, b1 = 0, b2 = 0, c1 = 0, c2 = 0, d1 = 0, d2 = 0;
    const char str[] = "100 500 -600 +700";
    const char fstr[] = "%lli %lli %lli %lli";

    int16_t res1 = s21_sscanf(str, fstr, &a1, &b1, &c1, &d1);
    int16_t res2 = sscanf(str, fstr, &a2, &b2, &c2, &d2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
    ck_assert_int_eq(b1, b2);
    ck_assert_int_eq(c1, c2);
    ck_assert_int_eq(d1, d2);
  }
  {
    long long a1 = 0, a2 = 0, b1 = 0, b2 = 0, c1 = 0, c2 = 0, d1 = 0, d2 = 0;
    const char str[] = "100500-600+700 123";
    const char fstr[] = "%lli %lld %lld %lli";

    int16_t res1 = s21_sscanf(str, fstr, &a1, &b1, &c1, &d1);
    int16_t res2 = sscanf(str, fstr, &a2, &b2, &c2, &d2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
    ck_assert_int_eq(b1, b2);
    ck_assert_int_eq(c1, c2);
    ck_assert_int_eq(d1, d2);
  }
  {
    long long a1 = 0, a2 = 0, c1 = 0, c2 = 0;
    char b1 = 0, b2 = 0, d1 = 0, d2 = 0;
    const char str[] = "100500-600+700+400";
    const char fstr[] = "%lli%c%lli%c";

    int16_t res1 = s21_sscanf(str, fstr, &a1, &b1, &c1, &d1);
    int16_t res2 = sscanf(str, fstr, &a2, &b2, &c2, &d2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
    ck_assert_int_eq(b1, b2);
    ck_assert_int_eq(c1, c2);
    ck_assert_int_eq(d1, d2);
  }
  {
    long long a1 = 0, a2 = 0, b1 = 0, b2 = 0, c1 = 0, c2 = 0, d1 = 0, d2 = 0;
    const char str[] = "  055555 0f 0f 0f5555555 0ddd   4    3    1 ";
    const char fstr[] = "%llo %lld %lld %lli";

    int16_t res1 = s21_sscanf(str, fstr, &a1, &b1, &c1, &d1);
    int16_t res2 = sscanf(str, fstr, &a2, &b2, &c2, &d2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
    ck_assert_int_eq(b1, b2);
    ck_assert_int_eq(c1, c2);
    ck_assert_int_eq(d1, d2);
  }
  {
    long long a1 = 0, a2 = 0, b1 = 0, b2 = 0, c1 = 0, c2 = 0, d1 = 0, d2 = 0;
    const char str[] = " 63DD 0xf 0xf 0xf5555555 ddd   4    3    1 ";
    const char fstr[] = "%lli %lld %lld %lli";

    int16_t res1 = s21_sscanf(str, fstr, &a1, &b1, &c1, &d1);
    int16_t res2 = sscanf(str, fstr, &a2, &b2, &c2, &d2);

    ck_assert_int_eq(res1, res2);
    ck_assert_int_eq(a1, a2);
    ck_assert_int_eq(b1, b2);
    ck_assert_int_eq(c1, c2);
    ck_assert_int_eq(d1, d2);
  }
}

Suite *sscanf_suite(void) {
  Suite *sscanf = suite_create("sscanf");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, sscanf_test_f);
  suite_add_tcase(sscanf, tc_core);

  return sscanf;
}

int sscanf_test() {
  int number_failed = 0;
  Suite *sscanf = sscanf_suite();
  SRunner *sr = srunner_create(sscanf);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
