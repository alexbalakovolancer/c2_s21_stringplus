#include "../test.h"

START_TEST(strerror_test_f) {
  for (int i = -3; i < 150; i++) {
    char *mac_os_errors[107] = {MACOS_ERRORS};
    char *linux_errors[134] = {LINUX_ERRORS};
    char *err_t = s21_strerror(i);
    char *err_r = strerror(i);
    ck_assert(err_t != NULL);  // Проверка на непустой указатель
    ck_assert(err_r != NULL);  // Проверка на непустой указатель
    ck_assert(strcmp(err_t, err_r) == 0);

    // Освобождайте память только для строк, выделенных с помощью malloc
    if (err_t != mac_os_errors[i] && err_t != linux_errors[i]) {
      free(err_t);
    }
  }
}
END_TEST

Suite *strerror_suite(void) {
  Suite *strerror = suite_create("strerror");
  TCase *tc_core = tcase_create("S21_STRING");

  tcase_add_test(tc_core, strerror_test_f);
  suite_add_tcase(strerror, tc_core);

  return strerror;
}

int strerror_test(void) {
  int number_failed = 0;
  Suite *strerror = strerror_suite();
  SRunner *sr = srunner_create(strerror);
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);

  return number_failed;
}
